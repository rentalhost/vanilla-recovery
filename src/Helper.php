<?php

declare(strict_types = 1);

namespace Rentalhost\VanillaRecovery;

class Helper
{
    /**
     * @param RecoveryAccess|string $password
     * @return RecoveryAccess|string
     */
    public static function passwordHash($password)
    {
        if ($password instanceof RecoveryAccess) {
            $password = $password->password;
        }

        $passwordStatus = password_get_info($password);

        if ($passwordStatus['algo'] === 0) {
            $hashOptions = [];

            if (\defined('VANILLA_RECOVERY_HASH_COST')) {
                $hashOptions['cost'] = VANILLA_RECOVERY_HASH_COST;
            }

            return (string) password_hash($password, PASSWORD_BCRYPT, $hashOptions);
        }

        return $password;
    }
}
