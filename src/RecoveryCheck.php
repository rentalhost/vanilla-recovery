<?php

declare(strict_types = 1);

namespace Rentalhost\VanillaRecovery;

use Rentalhost\VanillaResult\Result;

class RecoveryCheck
{
    /** @var string|null */
    private $originalPassword;

    /** @var int */
    private $validity;

    public function check(RecoveryAccess $yourRecoveryAccess, RecoveryAccess $expectedRecoveryAccess): Result
    {
        if ($yourRecoveryAccess->token !== $expectedRecoveryAccess->token) {
            return new Result(false, 'token.invalid', [
                'received' => $yourRecoveryAccess->token,
                'expected' => $expectedRecoveryAccess->token
            ]);
        }

        if ($this->validity !== null) {
            $validityDelta      = $this->validity * 3600;
            $validityDifference = $yourRecoveryAccess->timestamp - ($expectedRecoveryAccess->timestamp + $validityDelta);

            if ($validityDifference >= 0) {
                return new Result(false, 'timestamp.expired', [
                    'received'   => $yourRecoveryAccess->timestamp,
                    'expiredAt'  => $expectedRecoveryAccess->timestamp + $validityDelta,
                    'difference' => $validityDifference
                ]);
            }
        }

        if (password_verify($yourRecoveryAccess->password, $expectedRecoveryAccess->getHash())) {
            return new Result(true, 'success', [
                'recovered' => true
            ]);
        }

        if ($this->originalPassword && password_verify($yourRecoveryAccess->password, $this->originalPassword)) {
            return new Result(true, 'success', [
                'recovered' => false
            ]);
        }

        return new Result(false, 'password.incorrect');
    }

    public function getValidity(): ?int
    {
        return $this->validity;
    }

    public function setValidity(?int $validity): void
    {
        $this->validity = (int) $validity;
    }

    public function isOriginalPasswordAllowed(): bool
    {
        return (bool) $this->originalPassword;
    }

    /**
     * @param RecoveryAccess|string|null $originalPassword
     */
    public function setOriginalPassword($originalPassword): void
    {
        if ($originalPassword === null) {
            $this->originalPassword = null;

            return;
        }

        $this->originalPassword = Helper::passwordHash($originalPassword);
    }
}
