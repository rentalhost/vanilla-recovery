<?php

declare(strict_types = 1);

namespace Rentalhost\VanillaRecovery;

class RecoveryAccess
{
    /** @var string */
    public $hash;

    /** @var string */
    public $password;

    /** @var int */
    public $timestamp;

    /** @var string */
    public $token;

    public function __construct(?string $password = null, ?string $token = null, ?int $timestamp = null, ?string $hash = null)
    {
        $this->password  = (string) $password;
        $this->token     = $token !== null ? (string) $token : md5(random_bytes(255));
        $this->timestamp = $timestamp !== null ? (int) $timestamp : time();
        $this->hash      = $hash;
    }

    public static function generate(?string $password = null): RecoveryAccess
    {
        if ($password === null) {
            $password = random_bytes(12);
        }

        return new self($password);
    }

    public function getHash(): ?string
    {
        if ($this->password && !$this->hash) {
            $this->hash = Helper::passwordHash($this->password);
        }

        return $this->hash;
    }
}
