<?php

declare(strict_types = 1);

namespace Rentalhost\VanillaRecovery\Test;

use Rentalhost\VanillaRecovery\RecoveryAccess;

class RecoveryAccessTest extends Base
{
    /**
     * @covers \Rentalhost\VanillaRecovery\RecoveryAccess::__construct
     */
    public function testConstruct(): void
    {
        $recoveryAccess = new RecoveryAccess;

        static::assertNull($recoveryAccess->hash);
        static::assertNotNull($recoveryAccess->password);
        static::assertNotNull($recoveryAccess->token);
        static::assertNotNull($recoveryAccess->timestamp);

        $recoveryAccess = new RecoveryAccess('123', '456', 789);

        static::assertNull($recoveryAccess->hash);
        static::assertSame('123', $recoveryAccess->password);
        static::assertSame('456', $recoveryAccess->token);
        static::assertSame(789, $recoveryAccess->timestamp);
    }

    /**
     * @covers \Rentalhost\VanillaRecovery\RecoveryAccess::generate
     */
    public function testGenerate(): void
    {
        $recoveryAccess = RecoveryAccess::generate();

        static::assertSame(12, \strlen($recoveryAccess->password));

        $recoveryAccess = RecoveryAccess::generate('123456');

        static::assertSame('123456', $recoveryAccess->password);
    }

    /**
     * @covers \Rentalhost\VanillaRecovery\RecoveryAccess::getHash
     */
    public function testGetHash(): void
    {
        $recoveryAccess = RecoveryAccess::generate('');

        static::assertNull($recoveryAccess->getHash());

        $recoveryAccess = RecoveryAccess::generate();

        static::assertNotNull($recoveryAccess->getHash());
    }

    /**
     * @coversNothing
     */
    public function testPublicProperties(): void
    {
        static::assertClassHasAttribute('hash', RecoveryAccess::class);
        static::assertClassHasAttribute('password', RecoveryAccess::class);
        static::assertClassHasAttribute('timestamp', RecoveryAccess::class);
        static::assertClassHasAttribute('token', RecoveryAccess::class);
    }
}
