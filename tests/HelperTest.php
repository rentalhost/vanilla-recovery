<?php

declare(strict_types = 1);

namespace Rentalhost\VanillaRecovery\Test;

use Rentalhost\VanillaRecovery\Helper;
use Rentalhost\VanillaRecovery\RecoveryAccess;

class HelperTest extends Base
{
    /**
     * @covers \Rentalhost\VanillaRecovery\Helper::passwordHash
     */
    public function testPasswordHash(): void
    {
        $passwordPlain  = '123456';
        $passwordHash   = Helper::passwordHash($passwordPlain);
        $passwordRehash = Helper::passwordHash($passwordHash);

        static::assertTrue(password_verify($passwordPlain, $passwordHash));
        static::assertTrue(password_verify($passwordPlain, $passwordRehash));

        $recoveryAccess             = new RecoveryAccess($passwordRehash);
        $passwordFromRecoveryAccess = Helper::passwordHash($recoveryAccess);

        static::assertTrue(password_verify($passwordPlain, $passwordFromRecoveryAccess));
    }
}
